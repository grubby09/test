import discord
from discord.ext import commands

class Ban(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def ban(self, ctx, Member: discord.Member=None, reason=None):
        await ctx.send("Benutzer " + Member.mention + " wurde gebant")
        await Member.ban()

def setup(bot):
    bot.add_cog(Ban(bot))