import discord
from discord.ext import commands

class Kick(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def kick(self, ctx, Member: discord.Member=None, reason=None):
        await ctx.send("Benutzer " + Member.mention + " wurde gekickt")
        await Member.kick(reason=reason)

def setup(bot):
    bot.add_cog(Kick(bot))