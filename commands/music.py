import asyncio
import discord


from discord import Guild
from discord.ext import commands
import asyncio
import json
import os
import traceback
import datetime
from datetime import datetime
import discord
import youtube_dl
from discord.ext import commands
from discord import voice_client
from youtube_search import YoutubeSearch
global steuerung

class music(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name='p', aliases=['play'])
    async def play(self, ctx, *, Suche):
        ctx = ctx
        await ctx.message.delete()
        if ctx.author.voice is None:
            await ctx.send(f'{ctx.author.mention} joine einem Voicechannel um Musik mit mir zu hören', delete_after=4)
            return
        youtube = self.bot.get_emoji(822547498694082630)
        embed_search = discord.Embed(title=f'🔍searching {youtube}YouTube™ for:',
                                     description=f'**`{Suche}`**',
                                     color=discord.Color.random())
        await ctx.channel.purge(before=ctx.message, limit=1)
        await ctx.send(embed=embed_search)
        ydl_opts = {
           'format': 'bestaudio/best',
           'postprocessors': [{
           'key': 'FFmpegExtractAudio',
           'preferredcodec': 'mp3',
           'preferredquality': '192'
            }]
         }

        search = Suche
        try:
            yt = YoutubeSearch(search, max_results=1).to_json()
        except:
            print(traceback.print_exc())
            print("no results")
            return
        yt_id = str(json.loads(yt)['videos'][0]['id'])
        yt_url = 'https://www.youtube.com/watch?v=' + yt_id
        with youtube_dl.YoutubeDL(ydl_opts) as ydl:
            info = ydl.extract_info(yt_url, download=False)
            Titel = info.get('ttle')
            songname = f'songs/{str(ctx.guild.id)}{Titel} - {yt_id}.mp3'
            song_there = os.path.isfile(songname)
            if not song_there:
                print(f'Song wird gedownloadet:\n' f'{songname}')
                ydl.download([yt_url])
                for file in os.listdir('./'):
                    if file.endswith('.mp3'):

                        os.rename(file, songname)
            elif song_there:
                print(f'Song ist bereits gedownloadet\n{songname}')

        nichts = discord.Embed(title='Now plaing',
                            description='momentan wird nichts wiedergegeben da der Song oder die Queue zuende ist,\n'
                                        'oder du die Wiedergabe gestopt hast.\n'
                                        f'Gib  `.p "dein Suchbegriff"`  ein um nach einem neuen Song zu suchen',
                            color=discord.Color.random())
        nichts.set_thumbnail(
            url='https://cdn.discordapp.com/attachments/751133938361172060/813021258361995294/Lautsprecher.png')
        url= info.get("thumbnail")
        em = discord.Embed(title='Now playing',
                           description=f'{info.get("title")} [{ctx.author.mention}]',
                           colour=0x101295)
        em.set_footer(text=f'{str(json.loads(yt)["videos"][0]["views"])} 👁️\n'
                           f'Länge {str(json.loads(yt)["videos"][0]["duration"])}')
        em.set_image(url=url)
        m = await ctx.send(embed=em)
        voice_channel = ctx.author.voice.channel
        voice = ctx.channel.guild.voice_client
        if voice is None:
            voice = await voice_channel.connect()
        elif voice.channel != voice_channel:
            voice = await voice.move_to(voice_channel)
        if voice.is_playing:
            voice.stop()
        await asyncio.sleep(1)
        voice.play(discord.FFmpegPCMAudio(songname), after=lambda e: print("Song zuende"))
        await m.add_reaction("⏹️")
        #await m.add_reaction("⏭️")
        #await m.add_reaction("🔁")
        await m.add_reaction("⏸️")
        print(info.get('title'), f'wird wiedergegeben\n{yt_url}')

@commands.Cog.listener()
async def on_reaction_add(self, reaction, user):
    if not user.bot:
        if reaction.message.author == bot.user:
            nichts = discord.Embed(title='Now playing',
                                   description='momentan wird nichts wiedergegeben da der Song oder die Queue zuende ist,\n'
                                               'oder du die Wiedergabe gestopt hast.\n'
                                               f'Gib  `.p "dein Suchbegriff"`  ein um nach einem neuen Song zu suchen',
                                   color=discord.Color.random())
            nichts.set_thumbnail(
                url='https://cdn.discordapp.com/attachments/751133938361172060/813021258361995294/Lautsprecher_.png')
            if reaction.message.channel.guild.voice_client.channel:
                voice = reaction.message.channel.guild.voice_client.channel.name
            if user.voice:
                voice_channel = user.voice.channel.name
            else:
                voice_channel = None
            if not voice is voice_channel:
                await reaction.message.remove_reaction(reaction.emoji, user)
                await reaction.message.channel.send(f'{user.mention} joine dem Voice Channel in dem {bot.user.mention} ist um die Wiedergabe zu steuern',
                                            delete_after=5)
                return
            if voice is voice_channel:
                print(f'{reaction}')
                voice = reaction.message.channel.guild.voice_client
                if voice is None:
                    return
                else:
                    if reaction.emoji == ("⏸️"):
                        voice.pause()
                        await reaction.message.channel.send(embed=discord.Embed(title='The Song is curently paused',
                                                                                description='press the ▶️ Reaction to resume',
                                                                                color=discord.Color.random()))
                        await reaction.message.add_reaction('▶️')
                        await reaction.message.clear_reaction("⏸️")
                    paused= await reaction.message.channel.history(limit=1).find(lambda h: h.author == bot.user)
                    if reaction.emoji == ("▶️"):
                        await reaction.message.clear_reaction("▶️")
                        voice.resume()
                        await reaction.message.add_reaction("⏸️")
                        await paused.delete()
                    if reaction.emoji == ("⏹️"):
                        voice.stop()
                        await reaction.message.edit(embed= nichts)
                    try:
                        await reaction.message.remove_reaction(reaction.emoji, user)
                        return
                    except:
                        pass
        else:
            return

@commands.command(aliases= ['pa', ' pause', '⏸️','stop'])
async def pause(self, ctx):
        voice = ctx.channel.guild.voice_client
        if voice.is_playing():
            voice.pause()
            await ctx.message.delete()
            paused = discord.Embed(title='Song is paused',
                                   description=f'enter **`.re`** to resume',
                                   color=discord.Color.random())
            paused = await ctx.send(embed=paused)
            await paused.add_reaction('▶️')

        else:
            await ctx.send(f'{ctx.author.mention} Song is already paused', delete_after=3)
            await ctx.message.delete()

@commands.command(aliases= ['re', ' resume', 'continue', 'weiter', '▶️'])
async def resume(self, ctx):
        voice = ctx.channel.guild.voice_client
        try:
            if voice.is_paused():
                await ctx.channel.purge(before=ctx.message,limit=1)
                await ctx.message.delete()
                voice.resume()
            else:
                await ctx.message.delete()
                return
        except:
            await ctx.message.delete()
            return



def setup(bot):
    bot.add_cog(music(bot))