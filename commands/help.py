import discord
from discord.ext import commands
import json

with open("GrubbyGamer.json", "r") as f:
    data = json.load(f)

class Help(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def help(self, ctx, *, page=None):
        if page is None:
            embed1 = discord.Embed(title="Hilfe für GrubbyGamer", color=discord.Color.random())
            embed1.add_field(name="Allgemein", value="Zeigt die allgemeinen befehle an", inline=False)
            embed1.add_field(name="Moderation", value="Zeigt dir die Moderations Befehle an", inline=False)
        elif page == "Allgemein":
            embed2 = discord.Embed(title="Allgemeine Hilfe für denn GrubbyGamer")
            embed2.add_field(name="ping", value="Zeigt mein ping an", inline=False)
            embed2.add_field(name="userinfo", value="Zeigt die userinfo von ein User", inline=False)
            embed2.add_field(name="serverinfo", value="Zeigt die info über denn Discord Server", inline=False)
            await ctx.send(embed=embed2)
        elif page == "Moderation":
            embed3 = discord.Embed(title="Moderations Hilfe für denn GrubbyGamer")
            embed3.add_field(name="ban", value="bant ein User", inline=False)
            embed3.add_field(name="kick", value="kickt ein User", inline=False)
            embed3.add_field(name="unban", value="entbant ein User", inline=False)
            embed3.add_field(name="mute", value="muted ein User", inline=False)
            embed3.add_field(name="unmute", value="entmuted ein User", inline=False)
            await ctx.send(embed=embed3)

        await ctx.send(embed=embed1)

def setup(bot):
    bot.add_cog(Help(bot))

